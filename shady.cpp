
#define GL_GLEXT_PROTOTYPES 1
extern "C" {
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/freeglut.h>
#include <sys/time.h>
#include <unistd.h>
}

// Number of frames to use to calculate average fps.
static const int FPS_SMOOTHING = 20;

// Safety zone to use when warping the mouse pointer.
static const int MOUSE_BORDER = 40;

// Uncomment to visualize normals.
//#define DRAW_NORMALS

#include "shady.hpp"
#include "transform.hpp"
#include "output.hpp"
#include "world.hpp"
#include "renderer.hpp"

#include <cstdlib>
#include <ctime>

#include <string>
#include <vector>
using std::string;
using std::vector;

#include <cassert>

#include <boost/lexical_cast.hpp>
using boost::lexical_cast;

#include <sstream>
using std::stringstream;

#include <boost/foreach.hpp>


class Shady::_Private {
	
	friend class Shady;
	
	_Private(const vector<string> & _pps) : renderer(_pps) {
		posx = posy = 0.0;
		posz = 1.0;
		viewx = viewy = viewz = 0.0;
		
		radius = 1.0;
		var = 1.0;
		
		warped = false;
	}
	
	~_Private() {
	}
	
	Renderer renderer;
	
	World world;
	
	/**
	 * Move the player x units towards the viewx direction and y units to the right.
	 */
	void movePlayer(float x, float y);
	
	// Player position.
	float posx;
	float posy;
	float posz;
	
	// View rotation.
	float viewx;
	float viewy;
	float viewz;
	
	void renderHud(float tshadow, float tscene, float tpp);
	
	GLuint width;
	GLuint height;
	
	float radius;
	float var;
	
	// To be able to tell how much the mouse moved.
	int mousePosX;
	int mousePosY;
	
	bool warped;
	
};

Shady::Shady(const vector<string> & pps) : _p(new _Private(pps)) {
}

Shady::~Shady() {
	delete _p;
}

bool Shady::init() {
	
	_p->width = glutGet(GLUT_WINDOW_WIDTH);
	_p->height = glutGet(GLUT_WINDOW_HEIGHT);
	
	if(!_p->renderer.init(_p->width, _p->height)) {
		ERROR("initializing the rendering system");
		return false;
	}
	
	_p->renderer.resized(_p->width, _p->height);
	
	if(!_p->world.init()) {
		ERROR("initializing the world system");
		return false;
	}
	
	// generate an initial world
	_p->world.generate();
	
	return true;
}

void Shady::resized(int w, int h) {
	
	_p->width = w;
	_p->height = h;
	
	_p->renderer.resized(w, h);
	
}

static void renderBitmapString(float x, float y, void * font, const string & str) {
	glRasterPos2i(x, y);
	BOOST_FOREACH(char c, str) {
		glutBitmapCharacter(font, c);
	}
}

static string toString(float d) {
	stringstream ss;
	ss.precision(2);
	ss.setf(std::ios::fixed, std::ios::floatfield);
	ss << d;
	return ss.str();
}

float accurateTime() {
	timeval t;
	gettimeofday(&t, 0);
	return (float)t.tv_sec + (float)t.tv_usec/1000000.0;
}

float timeSinceStart() {
	static float start = accurateTime();
	return accurateTime() - start;
}

void Shady::_Private::renderHud(float tshadow, float tscene, float tpp) {
	
	tshadow *= 1000;
	tscene *= 1000;
	tpp *= 1000;
	
	glDisable(GL_DEPTH_TEST);
	
	glUseProgram(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, width, height, 0, -1, 1);
	
	//get time and fps
	static float lasttimes[FPS_SMOOTHING];
	float now = timeSinceStart();
	float fps = FPS_SMOOTHING / (now - lasttimes[FPS_SMOOTHING - 1]);
	for(int i= (FPS_SMOOTHING - 1); i >= 0; i--) {
		lasttimes[i] = lasttimes[i - 1];
	}
	lasttimes[0] = now;
	
	// Display values.
	glColor3d(1.0, 1.0, 1.0);
	const int yo = 15;
	const int ys = 14;
	int k = 0;
	
	//fix the "posz always 1 on hud" bug
	//FIXME this whole posz / realposz thing isn't so good
	float posz = this->posz + world.heightAt(posy, -posx);
	
#define STR(a) # a
#define SHOW(var) renderBitmapString(8, yo + (k++ * ys), GLUT_BITMAP_HELVETICA_12, STR(var) "=" + toString(var))
	
	SHOW(posx);
	SHOW(posy);
	SHOW(posz);
	SHOW(viewx);
	SHOW(viewy);
	SHOW(viewz);
	SHOW(radius);
	SHOW(var);
	SHOW(now);
	SHOW(fps);
	SHOW(tshadow);
	SHOW(tscene);
	SHOW(tpp);
	
	glEnable(GL_DEPTH_TEST);
	
	glPopMatrix();
	
}

void Shady::render() {
	
	float t1 = accurateTime();
	
	_p->renderer.renderShadowmap(_p->world);
	
	float t2 = accurateTime();
	
	transform camera = t_rotateZ(r(_p->viewz)) * t_rotateX(r(_p->viewy)) * t_rotateY(r(_p->viewx));
	camera *= t_translate(vec(-_p->posy, -(_p->posz + _p->world.heightAt(_p->posy, -_p->posx)), _p->posx));
	
	_p->renderer.renderScene(camera, _p->world);
	
	float t3 = accurateTime();
	
	_p->renderer.postprocess(_p->radius, _p->var, timeSinceStart());
	
	float t4 = accurateTime();
	
	_p->renderHud(t2 - t1, t3 - t2, t4 - t3);
	checkGl("rendering hud");
	
}

void Shady::mouseClicked(int button, int state, int x, int y) {
	
	// OUT("mouse clicked %d %d at %d,%d", button, state, x, y);
	
	if(button == GLUT_LEFT_BUTTON) {
		if(state == GLUT_DOWN) {
			_p->mousePosX = x;
			_p->mousePosY = y;
			glutSetCursor(GLUT_CURSOR_NONE);
		} else {
			assert(state == GLUT_UP);
			glutSetCursor(GLUT_CURSOR_INHERIT);
		}
	}
	
}

static float angle(float a) {
	while(a < -180.0) {
		a += 360.0;
	}
	while(a >= 180.0) {
		a -= 360.0;
	}
	return a;
}

void Shady::mouseDragged(int x, int y) {
	
	// OUT("mouse dragged from %d,%d to %d,%d", _p->mousePosX, _p->mousePosY, x, y);
	
	if(_p->warped) {
		// OUT("ignoring mouse move");
		_p->warped = false;
		return;
	}
	
	_p->viewx = angle(_p->viewx - (((float)(_p->mousePosX - x)) / 3.0));
	
	// Control viewy using arrow keys.
	_p->viewy = angle(_p->viewy - (((float)(_p->mousePosY - y)) / 3.0));
	
	// Keep mouse within the client area while dragging.
	if(x < MOUSE_BORDER || ((int)(_p->width) - x) < MOUSE_BORDER
	   || y < MOUSE_BORDER || ((int)(_p->height) - y) < MOUSE_BORDER) {
		int cx = _p->width / 2;
		int cy = _p->height / 2;
		// OUT("warping to %d,%d", cx, cy);
		_p->mousePosX = cx;
		_p->mousePosY = cy;
		_p->warped = true;
		glutWarpPointer(cx, cy);
	} else {
		_p->mousePosX = x;
		_p->mousePosY = y;
	}
	
}

#define POS_DELTA 0.3
#define HEIGHT_DELTA 0.3
#define VIEW_DELTA 2
#define ZOOM_FACTOR 1.05

#define R_DELTA 0.1
#define VAR_DELTA 0.1

void Shady::normalKeyPressed(unsigned char key, int x, int y) {
	
	// OUT("normal key: %c", key);
	
	switch(key) {
		
		case ' ':
			_p->world.generate();
			break;
		
		case '\b':
			_p->viewx = _p->viewy = _p->viewz = 0.0;
			_p->posx = _p->posy = 0.0;
			_p->posz = 1.0;
			break;
		
		case 'w':
		case 'W':
			_p->movePlayer(POS_DELTA, 0.0);
			break;
		
		case 's':
		case 'S':
			_p->movePlayer(-POS_DELTA, 0.0);
			break;
		
		case 'd':
		case 'D':
			_p->movePlayer(0.0, POS_DELTA);
			break;
		
		case 'a':
		case 'A':
			_p->movePlayer(0.0, -POS_DELTA);
			break;
		
		case 'q':
		case 'Q':
			_p->posz -= HEIGHT_DELTA;
			if(_p->posz < 0.0) {
				_p->posz = 0.0;
			}
			break;
		
		case 'e':
		case 'E':
			_p->posz += HEIGHT_DELTA;
			break;
		
		case '+':
		case '=':
			_p->radius += R_DELTA;
			break;
		
		case '-':
		case '_':
			_p->radius -= R_DELTA;
			break;
		
		case '*':
			_p->var += VAR_DELTA;
			break;
		
		case '/':
			_p->var -= VAR_DELTA;
			break;
		
	}
	
}

void Shady::specialKeyPressed(int key, int x, int y) {
	
	// OUT("special key: %d", key);
	
	switch(key) {
		
		case GLUT_KEY_UP:
			_p->viewy = angle(_p->viewy + VIEW_DELTA);
			break;
		
		case GLUT_KEY_DOWN:
			_p->viewy = angle(_p->viewy - VIEW_DELTA);
			break;
		
		case GLUT_KEY_RIGHT:
			_p->viewz = angle(_p->viewz + VIEW_DELTA);
			break;
		
		case GLUT_KEY_LEFT:
			_p->viewz = angle(_p->viewz - VIEW_DELTA);
			break;
		
	}
	
}

void Shady::_Private::movePlayer(float dx, float dy) {
	
	vec delta(dx, dy, 0.0);
	
	delta = t_rotateZ(r(viewx))(delta);
	
	this->posx += delta.x;
	this->posy += delta.y;
	
}
