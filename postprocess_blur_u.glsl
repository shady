
@@@ FRAGMENT SHADER

uniform sampler2D render;

uniform float radius;
uniform float var;

void main() {
	
	vec2 p = gl_TexCoord[0].st;
	
	const int n = 12;
	float r = radius * 0.025;
	
	float w = var;
	vec4 s = texture2D(render, p) * var;
	
	// float lt = (s.r + s.g + s.b) / 3;
	
	for(int i = -n; i <= n; i++) {
		vec2 off = vec2(r * i / n, 0);
		vec4 t = texture2D(render, p + off);
		float d = clamp(1.0 - (length(off) / r), 0.0, 1.0);
		//float l = (t.r + t.g + t.b) / 3;
		//l = clamp(l - lt, 0.0, 1.0);
		//float k = d * d * d * l * l * l;
		float k = d;
		s += t * k;
		w += k;
	}
	
	gl_FragColor = s / w;
}

