
@@@ VERTEX SHADER

varying vec4 shadowTexCoord;

varying vec3 lightCoord;
varying vec3 normal;
varying vec3 fragPos;

uniform vec3 lightPos;


void main() {
	
	gl_Position = ftransform();
	vec4 vertex = gl_ModelViewMatrix * gl_Vertex;
	fragPos = vertex.xyz;
	shadowTexCoord = gl_TextureMatrix[0] * vertex;
	
	gl_FrontColor = gl_Color;
	
	normal = gl_NormalMatrix * gl_Normal;
	
	lightCoord = lightPos - vertex.xyz;
	
}

@@@ FRAGMENT SHADER

uniform sampler2DShadow shadowmap;

varying vec4 shadowTexCoord;

varying vec3 lightCoord;
varying vec3 normal;
varying vec3 fragPos;

void main() {
	float ambient = 0.2;
	float diffuse = 1.0;
	float specular = 2;
	float shininess = 10;
	
	//diffuse light
	vec3 lightVec = normalize(lightCoord);
	vec3 normalVec = normalize(normal);
	float z = clamp(dot(normalVec,lightVec),0.0,1.0);
	float diffuseLight = diffuse*z;
	
	//specualr light (Blinn-Phong model)
	vec3 eyeVec = normalize(fragPos);
	vec3 reflectVec = normalize(-2*normalVec*dot(normalVec,lightVec)+lightVec);
	float y = dot(reflectVec,eyeVec);
	float specularLight = specular*z*pow(clamp(y,0.0,1.0),shininess);
	
	//shadows
	vec4 stc = shadowTexCoord / shadowTexCoord.w;
	
	int a = 12547831;
	a -= int(1254.2654 * fragPos.x);
	a ^= a<<7;
	a ^= a>>5;
	a += a<<3;
	a -= a>>6;
	a ^= int(1254.2654 * fragPos.y);
	a ^= a<<5;
	a += a>>3;
	a -= a<<2;
	a ^= a>>9;
	vec3 offset = vec3(2*float((a%347))/348 - 1, 2*float((a%1483))/1484 - 1, 0);
	//stc.z += 0.0000002;
	float shadow;
	for(int x = -3; x <= 3; x++) {
		for(int y = -3; y <= 3; y++) {
			shadow += shadow2D(shadowmap, stc.xyz + (vec3(x, y, 0) + 0.5* offset ) * 0.001).r;
		}
	}
	shadow /= 49;
	
	//total light
	float light = ambient + ( diffuseLight + specularLight ) * shadow;
	
	//light = 1.f; // TODO
	//float light = specularLight;
	vec4 color =  light * gl_Color;

	if((shadowTexCoord.w < 0 && shadowTexCoord.w > 0.0001) || (stc.x < 0 && stc.x > -0.001) || (stc.x > 1 && stc.x < 1.001) || (stc.y < 0 && stc.y > -0.001) || (stc.y > 1 && stc.y < 1.001)) {
		// Outside of the shadow map.
		color = vec4(2/length(lightCoord) + 0.5, 0.0, 0.0, 1.0);
	}
	
	gl_FragColor = color;
	
}
