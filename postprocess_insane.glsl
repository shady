
@@@ FRAGMENT SHADER

uniform sampler2D render;

uniform float radius;
uniform float time;
uniform float var;
uniform float aspect;


vec2 distortion(float t,vec2 p){
	float xd = 0.05*sin(3.14*p.y + 1.35*t) + 0.03*sin(11.23*p.x + 5.13 * t) + 0.005*sin(43*p.x + 8.33*t);
	float yd = 0.05*sin(4.02*p.x + 1.16*t) + 0.03*sin(10.74*p.y + 6.35 * t) + 0.005*sin(47*p.y + 8.33*t);;
	return radius * vec2(xd, yd * aspect);
}

void main() {
	
	vec2 p = gl_TexCoord[0].st;
	
	vec4 s = vec4(0,0,0,0);
	for(int i=0; i<20; i++){
		s += pow(1.8,-2)*texture2D(render, p+distortion(time-i*var/100,p));
	}

	gl_FragColor = s/5;
}

