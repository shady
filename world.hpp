
#ifndef SHADY_WORLD_HPP
#define SHADY_WORLD_HPP

#include "transform.hpp"

class transform;

class World {
	
public:
	
	World();
	
	~World();
	
	/**
	 * Initialize world datastructures.
	 */
	bool init();
	
	/**
	 * Render one frame.
	 */
	void render(vec pos) const;
	
	/**
	 * Generate a new world.
	 */
	void generate();
	
	float heightAt(float x, float y) const;
	
	/**
	 * Get the transformation from model space to light space.
	 **/
	const transform & getLightPosition() const;
	
private:
	
	class _Private;
	_Private * const _p;
	
};

#endif // SHADY_WORLD_HPP
