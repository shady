
@@@ FRAGMENT SHADER

uniform sampler2D render;


uniform float radius;
uniform float var;
uniform float time;

void main() {
	
	vec2 p = gl_TexCoord[0].st;
	vec4 color = texture2D(render, p);
	
	//generate noise
	int a = 12547831;
	a += int(1254.2654*time);
	a -= a<<4;
	a ^= a>>3;
	a ^= a<<9;
	a += a>>3;
	a -= int(1254.2654*p.x);
	a ^= a<<7;
	a ^= a>>5;
	a += a<<3;
	a -= a>>6;
	a ^= int(1254.2654*p.y);
	a ^= a<<5;
	a += a>>3;
	a -= a<<2;
	a ^= a>>9;
	
	vec4 colornoise = vec4(float((a%347))/348, float((a%853))/854, float((a%973))/974, 0.0);
	vec4 bwnoise = float((a%1483))/1484 * vec4(1,1,1,0);
	
	colornoise = colornoise * 2 - 1;
	bwnoise = bwnoise * 2 - 1;

	float v = var / 2;
	
	float darkness = (1 - ((color.r + color.g + color.b)/3)) * 0.8;
	darkness = clamp(darkness * darkness * darkness - 0.3, 0.0, 1.0);
	
	gl_FragColor = color + radius * darkness * ( (v * colornoise) + ((1-v)*bwnoise) );
}

