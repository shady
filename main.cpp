
// For initializing the random number generator.
#include <ctime>
#include <cstdlib>
using std::time;
using std::srand;

#include <string>
#include <vector>
using std::string;
using std::vector;

extern "C" {
#include <GL/freeglut.h>
}

#include <cassert>

#include "output.hpp"
#include "shady.hpp"


static Shady * shady;


static void render() {
	shady->render();
	glutSwapBuffers();
}

static void resized(int width, int height) {
	shady->resized(width, height);
}

static void normalKeyPressed(unsigned char key, int x, int y) {
	shady->normalKeyPressed(key, x, y);
}

static void specialKeyPressed(int key, int x, int y) {
	shady->specialKeyPressed(key, x, y);
}

static void mouseDragged(int x,int y) {
	shady->mouseDragged(x, y);
}

static void mouseClicked(int button, int state, int x, int y) {
	shady->mouseClicked(button, state, x, y);
}


int main(int argc, char ** argv) {
	
	OUT(OUT_SHELLCOMMAND(OUT_BRIGHT) "Shady - OpenGL playground" OUT_SHELLCOMMAND(OUT_RESET));
	
	OUT("usage: shady <postprocess>...");
	
	glutInit(&argc, argv);
	
	vector<string> pps;
	for(int i = 1; i < argc; i++) {
		pps.push_back(argv[i]);
	}
	
	// TODO generalize keybindings
	OUT("Movement: " OUT_COLORED(OUT_GREEN, "WASD"));
	OUT("Height: " OUT_COLORED(OUT_GREEN, "Q/E"));
	OUT("Rotation: " OUT_COLORED(OUT_GREEN, "mouse") " (drag) and " OUT_COLORED(OUT_GREEN, "arrow keys"));
	OUT("Radius: " OUT_COLORED(OUT_GREEN, "+/-"));
	OUT("Var: " OUT_COLORED(OUT_GREEN, "*") " and " OUT_COLORED(OUT_GREEN, "/"));
	OUT("Reset view: " OUT_COLORED(OUT_GREEN, "backspace"));
	OUT("New world: " OUT_COLORED(OUT_GREEN, "space"));
	
	// Initialize the random number generator for unique worlds.
	srand(time(NULL) / 2);
	
	shady = new Shady(pps);
	
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(1024, 860);
	
	glutCreateWindow("Shady");
	
	if(!shady->init()) {
		ERROR("initializing shady");
		delete shady;
		return 1;
	}
	
	// TODO disable key repeating for smoother movement
	
	glutDisplayFunc(render);
	glutIdleFunc(render);
	glutReshapeFunc(resized);
	glutKeyboardFunc(normalKeyPressed);
	glutSpecialFunc(specialKeyPressed);
	glutMouseFunc(mouseClicked);
	glutMotionFunc(mouseDragged);
	
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);
	
	glutMainLoop();
	
	// Cleanup.
	delete shady;
	
	OUT("clean exit");
	
	return 0;
}
