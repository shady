
@@@ FRAGMENT SHADER

uniform sampler2D render;

uniform float radius;
uniform float time;
uniform float var;
uniform float aspect;

void main() {
	
	vec2 p = gl_TexCoord[0].st;
	
	vec4 s = vec4(0, 0, 0, 0);
	for(int i = 0; i < 20; i++){
		s += pow(1.8, -2) * texture2D(render, ((1 - 0.01 * var * i) * (p - 0.5)) + 0.5);
	}
	
	gl_FragColor = s / 5;
}

