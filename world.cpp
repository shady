
#include "world.hpp"
#include "transform.hpp"
#include "terrain.hpp"
#include "output.hpp"
#include "renderer.hpp" // for checkGl

extern "C" {
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
}

#include <cstdlib>
using std::rand;


class World::_Private {
	
	friend class World;
	
	_Private() : trrn(512, 64, 9), dlist(GL_NONE), vbo(GL_NONE) {
	}
	
	~_Private() {
		if(dlist) {
			glDeleteLists(dlist, 1);
		}
		if(vbo) {
			glDeleteBuffers(1, &vbo);
		}
	}
	
	terrain trrn;
	
	// OpenGL display list containing the world
	GLuint dlist;
	
	GLuint vbo;
	
	// Transformation from model space to light space.
	transform lightPos;
	
};

World::World() : _p(new World::_Private()) {
}

World::~World() {
	delete _p;
}

#ifdef DRAW_NORMALS
static void drawNormals(const vec * p, int n, const vec & normal) {
	vec nn = normal / 5;
	for(int i = 0; i < n; i++) {
		glBegin(GL_LINES);
			glVertex3f(p[i].x, p[i].y, p[i].z);
			glVertex3f(p[i].x + nn.x, p[i].y + nn.y, p[i].z + nn.z);
		glEnd();
	}
}
#else
#define drawNormals(p,n,normal)
#endif

static void drawTrinagle(const vec p[3]) {
	
	vec n = calculateTriangleNormal(p);
	glNormal3f(n.x, n.y, n.z);
	
	glBegin(GL_TRIANGLES);
		for(int i = 0; i < 3; i++) {
			glVertex3f(p[i].x, p[i].y, p[i].z);
		}
	glEnd();
	
	drawNormals(p, 3, n);
	
}

static void drawQuad(const vec p[4]) {
	
	vec n = calculateNormal(p, 4);
	glNormal3f(n.x, n.y, n.z);
	
	glBegin(GL_QUADS);
		for(int i = 0; i < 4; i++) {
			glVertex3f(p[i].x, p[i].y, p[i].z);
		}
	glEnd();
	
	drawNormals(p, 4, n);
	
}

static void drawQuads(const vec p[][4], int n) {
	for(int i = 0; i < n; i++) {
		drawQuad(p[i]);
	}
}

static void drawTrinagles(const vec p[][3], int n) {
	for(int i = 0; i < n; i++) {
		drawTrinagle(p[i]);
	}
}

static const float w = 0.1; // width of objects (height=1.0)
static float p = sqrt(2 * w * w);

static void drawCuboid() {
	
	vec quads[6][4] = {
		
		// side faces
		{
			vec(w, 0, w),
			vec(w, 0, -w),
			vec(w, 1, -w),
			vec(w, 1, w)
		}, {
			vec(w, 1, w),
			vec(-w, 1, w),
			vec(-w, 0, w),
			vec(w, 0, w)
		}, {
			vec(-w, 1, w),
			vec(-w, 1, -w),
			vec(-w, 0, -w),
			vec(-w, 0, w)
		}, {
			vec(w, 0, -w),
			vec(-w, 0, -w),
			vec(-w, 1, -w),
			vec(w, 1, -w)
		},
		
		// top/bottom face
		{
			vec(w, 1, w),
			vec(w, 1, -w),
			vec(-w, 1, -w),
			vec(-w, 1, w)
		}, {
			vec(w, 0, w),
			vec(-w, 0, w),
			vec(-w, 0, -w),
			vec(w, 0, -w)
		}
		
	};
	
	drawQuads(quads, 5);
}

static void drawSquarePyramid() {
	
	vec triangles[5][3] = {
		
		{
			vec(w, 0, w),
			vec(w, 0, -w),
			vec(0, 1, 0)
		}, {
			vec(0, 1, 0),
			vec(-w, 0, w),
			vec(w, 0, w)
		}, {
			vec(0, 1, 0),
			vec(-w, 0, -w),
			vec(-w, 0, w)
		}, {
			vec(w, 0, -w),
			vec(-w, 0, -w),
			vec(0, 1, 0)
		},
		
	};
	
	drawTrinagles(triangles, 4);
	
	// bottom face
	vec quad[4] = {
		vec(w, 0, w),
		vec(-w, 0, w),
		vec(-w, 0, -w),
		vec(w, 0, -w)
	};
	
	drawQuad(quad);
}

static void drawTrianglePyramid() {
	
	vec triangles[4][3] = {
		
		{
			vec(0, 1, 0),
			vec(-w, 0, -w),
			vec(-w, 0, w)
		}, {
			vec(-w, 0, w),
			vec(p, 0, 0),
			vec(0, 1, 0)
		}, {
			vec(0, 1, 0),
			vec(p, 0, 0),
			vec(-w, 0, -w)
		},
		
		// bottom face
		{
			vec(-w, 0, w),
			vec(-w, 0, -w),
			vec(p, 0, 0)
		}
		
	};
	
	drawTrinagles(triangles, 3);
}

static void drawPrism() {
	
	// side faces
	vec quads[3][4] = {
		
		{
			vec(-w, 1, w),
			vec(-w, 1, -w),
			vec(-w, 0, -w),
			vec(-w, 0, w)
		}, {
			vec(-w, 0, w),
			vec(p, 0, 0),
			vec(p, 1, 0),
			vec(-w, 1, w)
		}, {
			vec(-w, 1, -w),
			vec(p, 1, 0),
			vec(p, 0, 0),
			vec(-w, 0, -w)
		}
		
	};
	drawQuads(quads, 3);
	
	// top/bottom face
	vec triangles[2][3] = {
		
		{
			vec(-w, 1, w),
			vec(p, 1, 0),
			vec(-w, 1, -w)
		}, {
			vec(-w, 0, w),
			vec(-w, 0, -w),
			vec(p, 0, 0)
		}
		
	};
	
	drawTrinagles(triangles, 2);
}

void drawSphere() {
	
	//create sphere
	GLUquadric * quadr =  gluNewQuadric();
	glTranslatef(0, 0.2, 0);
	gluSphere(quadr, 0.2, 20, 20);
	glTranslatef(0, -0.2, 0);
	gluDeleteQuadric(quadr);
}


void World::render(vec pos) const {
	
	glBindBuffer(GL_ARRAY_BUFFER, _p->vbo);
	_p->trrn.drawVertices(pos);
	
	checkGl("rendering terrain");
	
	glCallList(_p->dlist);
	
	checkGl("rendering objects");
}

void World::generate() {
	
	int trrnRandRes = rand() % 2 ? 4 : 8; // 8 or 16
	float trrnDiv = 1.35 + 0.1*(float)(rand()%1024)/1024; // 1.35-1.45
	float trrnExp = 0.7 + 1.3*(float)(rand()%1024)/1024; //0.7-2
	int trrnSeed = rand();
	_p->trrn.generate(trrnRandRes,trrnDiv,trrnExp,trrnSeed);
	
	glBindBuffer(GL_ARRAY_BUFFER, _p->vbo);
	_p->trrn.generateVertices();
	
	//create world
	glNewList(_p->dlist, GL_COMPILE);
	
	static const float a = 10.0; // width/height of area with objects
	
	/*// Floor
	glNormal3f(0, 1, 0);
	glBegin(GL_QUADS);
		glColor3d(0.3, 0.3, 0.5);
		glVertex3d(10 * a, 0.0, 10 * a);
		glColor3d(0.3, 0.5, 0.3);
		glVertex3d(10 * a, 0.0, 10 * -a);
		glColor3d(0.3, 0.3, 0.3);
		glVertex3d(10 * -a, 0.0, 10 * -a);
		glColor3d(0.5, 0.3, 0.3);
		glVertex3d(10 * -a, 0.0, 10 * a);
	glEnd();*/
	
	//_p->trrn.drawGl();
	
	// Create some random objects.
	int nitems = (int)randVar(40.0, 5.0);
	for(int i = 0; i < nitems; ++i) {
		
		glPushMatrix();
		
		float x = randVal(-a, a);
		float y = randVal(-a, a);
		float z = heightAt(x, y);
		glTranslatef(x, z, y);
		
		float scale = randVar(2.5, 1.0);
		float scale2 = randVar(0.75 * scale, 0.5);
		glScalef(scale, scale2, scale);
		
		glRotatef(randVal(0.0, 360.0), 0.0, 1.0, 0.0);
		
		glColor3f(randVal(0.0,1.0), randVal(0.0,1.0), randVal(0.0,1.0));
		
		float type = randVal();
		
		if(type >= 0.8) {
			drawCuboid();
		} else if(type >= 0.60) {
			drawSquarePyramid();
		} else if(type >= 0.4) {
			drawPrism();
		} else if(type >= 0.2) {
			drawSphere();
		} else {
			drawTrianglePyramid();
		}
		
		glPopMatrix();
		
	}
	
	glEndList();
	
	checkGl("generating new world");
	
	float rx = r(-135.0);
	float ry = r(40.0);
	float rz = r(0.0);
	float tx = 10.0;
	float ty = 10.0;
	float tz = 4.0 + heightAt(ty, -tx);
	
	_p->lightPos = t_rotateZ(rz) * t_rotateX(ry) * t_rotateY(rx) * t_translate(vec(-ty, -tz, tx));
	
}

float World::heightAt(float x, float y) const {
	return _p->trrn.heightAt(x, y);
}

bool World::init() {
	_p->dlist = glGenLists(1);
	if(!_p->dlist) {
		GLenum error = glGetError();
		ERROR("generating world display list: %d: %s", error, gluErrorString(error));
		return false;
	}
	glGenBuffers(1, &_p->vbo);
	if(!_p->vbo) {
		GLenum error = glGetError();
		ERROR("generating terrain VBO: %d: %s", error, gluErrorString(error));
		return false;
	}
	return true;
}

const transform & World::getLightPosition() const {
	return _p->lightPos;
}
