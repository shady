
#ifndef SHADY_TRANSFORM_HPP
#define SHADY_TRANSFORM_HPP

#include <cmath>

inline float round(float value) {
	return std::floor(value + 0.5);
}

float randVal();

float randVar(float val, float var);

int randNextInt(float val);

float randVal(float min, float max);

#define PI 3.14159265

inline float r(float degrees) {
	return degrees*PI/180;
}

/** Vector */

struct vec {
	
	float x;
	float y;
	float z;
	
	inline vec(float vx, float vy, float vz) : x(vx), y(vy), z(vz)  { }
	
	inline vec(const vec & o) : x(o.x), y(o.y), z(o.z) { }
	
	inline vec() : x(0.0), y(0.0), z(0.0) { }
	
	inline vec & translate(float dx, float dy, float dz) {
		x += dx;
		y += dy;
		z += dz;
		return *this;
	}
	
	inline vec & operator+=(const vec & t) {
		x += t.x;
		y += t.y;
		z += t.z;
		return *this;
	}
	
	inline vec & operator-=(const vec & t) {
		x -= t.x;
		y -= t.y;
		z -= t.z;
		return *this;
	}
	
	inline vec operator-() const {
		return vec(-x, -y, -z);
	}
	
	inline vec operator+(const vec & o) const {
		return vec(x + o.x, y + o.y, z + o.z);
	}
	
	inline vec operator-(const vec & o) const {
		return vec(x - o.x, y - o.y, z - o.z);
	}
	
	inline vec & operator*=(float f) {
		x *= f;
		y *= f;
		z *= f;
		return *this;
	}
	
	inline vec operator*(float f) const {
		return vec(x * f, y * f, z * f);
	}
	
	inline vec & operator/=(float f) {
		return operator*=(1/f);
	}
	
	inline vec operator/(float f) const {
		return operator*(1/f);
	}
	
	inline float det() {
		return std::sqrt((x * x) + (y * y) + (z * z));
	}
	
	inline vec normal() {
		float t = det();
		if(t != 0)
			return vec(x / t, y / t, z / t);
		return vec();
	}
	
};

inline vec v_null() { return vec(0.0, 0.0, 0.0); }
inline vec v_x() { return vec(1.0, 0.0, 0.0); }
inline vec v_y() { return vec(0.0, 1.0, 0.0); }
inline vec v_z() { return vec(0.0, 0.0, 1.0); }


/** Matrix */

struct mat {
	
	float d[3][3];
	
	inline mat(float d00, float d01, float d02, float d10, float d11,
	           float d12, float d20, float d21, float d22) {
		d[0][0] = d00; d[0][1] = d01; d[0][2] = d02;
		d[1][0] = d10; d[1][1] = d11; d[1][2] = d12;
		d[2][0] = d20; d[2][1] = d21; d[2][2] = d22;
	}
	
	mat(const mat & o);
	
	/**
	 * Calculate this * v
	 */
	vec operator()(const vec & v) const;
	
	/**
	 * Calculate v * this
	 */
	vec operator[](const vec & v) const;
	
	/**
	 * Calculate this * b
	 */
	mat operator*(const mat & b) const;
	
	inline mat & operator*=(const mat & b) {
		return *this = *this * b;
	}
	
	/**
	 * Calculate the transposed matrix.
	 */
	mat operator-() const;
	
};

mat m_null();
mat m_id();


/** Affine Transformation */

struct transform {
	
	mat r;
	vec t;
	
	transform(const mat & rot, const vec & trans);
	
	transform(const transform & other);
	
	transform();
	
	transform(const mat & rot);
	
	transform(const vec & trans);
	
	inline transform & translate(vec _t) {
		t += _t;
		return *this;
	}
	
	inline transform & pretranslate(vec _t) {
		t += r(_t);
		return *this;
	}
	
	inline vec operator()(const vec & p) const {
		return r(p) + t;
	}
	
	inline vec operator[](const vec & p) const {
		return r[p - t];
	}
	
	inline transform operator-() const {
		return transform(-r, -(r[t]));
	}
	
	/**
	 * Calculate the inverse transformation. m must be a valid rotation matrix.
	 */
	transform & invert();
	
	inline transform operator*(const transform & b) const {
		return transform(r * b.r, (*this)(b.t));
	}
	
	inline transform & operator*=(const transform & b) {
		t += r(b.t);
		r *= b.r;
		return *this;
	}
	
};


inline transform t_translate(const vec & t) {
	return transform(t);
}

transform t_rotateX(float theta);

transform t_rotateY(float theta);

transform t_rotateZ(float theta);

transform t_rotate(const vec & axis, float theta);


vec calculateTriangleNormal(const vec p[3]);

vec calculateNormal(const vec * p, int n);

#endif // SHADY_TRANSFORM_HPP
