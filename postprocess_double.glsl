
@@@ FRAGMENT SHADER

uniform sampler2D render;


uniform float radius;
uniform float var;
uniform float time;

void main() {
	vec2 p = gl_TexCoord[0].st;
	
	vec2 diff = 0.02*radius*vec2(sin(time*1.08246),cos(time*1.0356234));
	vec2 dist1 = var * (vec2(sin(time*0.32345), cos(time*0.23874)) + 1) / 4 + 0.5;
	vec2 p1 = (p - 0.5) * dist1 + 0.5;
	vec2 dist2 = var * (vec2(sin(time*0.28362), cos(time*0.33289)) + 1) / 4 + 0.5;
	vec2 p2 = (p - 0.5) * dist2 + 0.5;
	gl_FragColor = (texture2D(render,p1+diff)+texture2D(render,p2-diff))/2;
}

