
#ifndef SHADY_OUTPUT_HPP
#define SHADY_OUTPUT_HPP

extern int debug_depth;

#include <cstdio>
#define _OUT_PRINT std::fprintf

#define _OUT_BRACKET(str) OUT_COLORED(OUT_CYAN, str)

#define OUT_OPEN(F,...) _OUT_PRINT(stdout,"%*c" F " " _OUT_BRACKET("{") "\n",(debug_depth++)*2,' ',##__VA_ARGS__)
#define OUT(F,...)      _OUT_PRINT(stdout,"%*c" F "\n",(debug_depth)*2,' ',##__VA_ARGS__)
#define OUT_CLOSE()     _OUT_PRINT(stdout,"%*c" _OUT_BRACKET("}") "\n",(--debug_depth)*2,' ')
#define OUT_START()     _OUT_PRINT(stdout,"%*c",(debug_depth)*2,' ')
#define OUT_END()       _OUT_PRINT(stdout,"\n")
#define OUT_PART(...)   _OUT_PRINT(stdout,__VA_ARGS__)

#define OUT_RESET      "0"
#define OUT_BRIGHT     "1"
#define OUT_DIM        "2"
#define OUT_UNDERLINE  "3"
#define OUT_BLINK      "5"
#define OUT_REVERSE    "7"
#define OUT_HIDDEN     "8"

#define OUT_BLACK      "0"
#define OUT_RED        "1"
#define OUT_GREEN      "2"
#define OUT_YELLOW     "3"
#define OUT_BLUE       "4"
#define OUT_MAGENTA    "5"
#define OUT_CYAN       "6"
#define OUT_WHITE      "7"

#define OUT_FG "3"
#define OUT_BG "4"

#define OUT_SHELLCOMMAND(command) "\x1B[" command "m"

#define OUT_FGCOLOR(color) OUT_SHELLCOMMAND(OUT_FG color)
#define OUT_COLOR_RESET OUT_SHELLCOMMAND(OUT_RESET)

#define OUT_COLORED(color, str) OUT_FGCOLOR(color) str OUT_COLOR_RESET
#define OUT_SEP ";"

#define ERROR_COLOR      OUT_RED OUT_SEP OUT_BRIGHT
#define ERROR(F,...)     _OUT_PRINT(stderr,"%*c" OUT_COLORED(ERROR_COLOR, "ERROR: " F) "\n",(debug_depth)*2,' ',##__VA_ARGS__)
#define WARNING_COLOR    OUT_YELLOW OUT_SEP OUT_BRIGHT
#define WARNING(F,...)   _OUT_PRINT(stderr,"%*c" OUT_COLORED(WARNING_COLOR, "WARNING: " F) "\n",(debug_depth)*2,' ',##__VA_ARGS__)

#endif // SHADY_OUTPUT_HPP
