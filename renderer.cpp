
#define GL_GLEXT_PROTOTYPES 1
extern "C" {
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glu.h>
}

// Size of the shadow map.
static const int SHADOWMAP_SIZE = 4098;


#include "renderer.hpp"
#include "transform.hpp"
#include "output.hpp"
#include "world.hpp"

#include <string>
#include <vector>
#include <iostream>
using std::string;
using std::vector;

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
namespace fs = boost::filesystem;

#include <boost/foreach.hpp>

using std::cout;
using std::endl;


class Renderer::_Private {
	
	friend class Renderer;
	
	_Private(const vector<string> & _pps) : pps(_pps) {
		shadow_fbo = 0;
		shadow_map = 0;
		shader_render = 0;
		pp_fbo = 0;
		pp_rb = 0;
		pp_tex[0] = 0;
		pp_tex[1] = 0;
	}
	
	~_Private() {
		if(shadow_fbo) {
			glDeleteFramebuffersEXT(1, &shadow_fbo);
		}
		glDeleteTextures(1, &shadow_map);
		if(shader_render) {
			glDeleteObjectARB(shader_render);
		}
		if(pp_fbo) {
			glDeleteFramebuffersEXT(1, &pp_fbo);
		}
		if(pp_rb) {
			glDeleteRenderbuffersEXT(1, &pp_rb);
		}
		glDeleteTextures(2, pp_tex);
		BOOST_FOREACH(GLhandleARB shader, shader_pp) {
			glDeleteObjectARB(shader);
		}
		checkGl("cleanup");
	}
	
	World world;
	
	// Framebuffer object for rendering the shadow map.
	GLuint shadow_fbo;
	GLuint shadow_map;
	
	GLhandleARB shader_render;
	
	void generatePPTextures();
	
	// Framebuffer object to render into an immediate buffer to allow post processing.
	GLuint pp_fbo;
	GLuint pp_rb;
	GLuint pp_tex[2];
	
	GLuint width;
	GLuint height;
	
	std::vector<std::string> pps;
	std::vector<GLhandleARB> shader_pp;
	
	void renderPostprocessing(GLuint shader, float radius, float var, float time);
	
	GLfloat lightProj[16];
	
};

Renderer::Renderer(const vector<string> & pps) : _p(new _Private(pps)) {
}

Renderer::~Renderer() {
	delete _p;
}

bool checkGl(const char * action, bool fatal) {
	GLenum error = glGetError();
	if(error) {
		if(fatal) {
			ERROR("%s: %d: %s", action, error, gluErrorString(error));
		} else {
			WARNING("%s: %d: %s", action, error, gluErrorString(error));
		}
	}
	return !error;
}

void Renderer::_Private::generatePPTextures() {
	
	if(pps.empty()) {
		return;
	}
	
	glBindTexture(GL_TEXTURE_2D, pp_tex[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	
	if(pps.size() > 1) {
		glBindTexture(GL_TEXTURE_2D, pp_tex[1]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	}
	
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, pp_rb);
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24, width, height);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);
	
	checkGl("resizing textures");
	
}

static bool checkFBO(const char * name) {
	
	GLuint error = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	if(error != GL_FRAMEBUFFER_COMPLETE_EXT) {
		ERROR("creating %s FBO: driver doesn't support the FBO configuration %d: %s", name, error, glGetString(error));
		return false;
	}
	
	return true;
}

static void checkShader(GLuint object, const char * op, GLuint check, const fs::path & file) {
	
	GLint status;
	glGetObjectParameterivARB(object, check, &status);
	if(!status) {
		int logLength;
		glGetObjectParameterivARB(object, GL_OBJECT_INFO_LOG_LENGTH_ARB, &logLength);
		char * log = new char[logLength];
		glGetInfoLogARB(object, logLength, NULL, log);
		ERROR("%s shader %s: %s", op, file.string().c_str(), log);
		delete log;
		exit(1);
	}
	
}

static const string shaderSeperator = "@@@";
static const string vertShader = " VERTEX SHADER";
static const string fragmentShader = " FRAGMENT SHADER";

static GLuint loadShader(const fs::path & file) {
	
	if(!fs::exists(file)) {
		ERROR("cannot find shader file %s", file.string().c_str());
		exit(0);
	}
	
	// load the file
	fs::ifstream ifs(file);
	string src((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	
	size_t startpos = src.find(shaderSeperator);
	if(startpos == string::npos) {
		ERROR("loading shader \"%s\": no @@@ found", file.string().c_str());
		return 0;
	}
	startpos += shaderSeperator.length();
	
	GLuint shader = glCreateProgramObjectARB();
	
	while(startpos != string::npos) {
		
		GLuint type;
		const char * error;
		if(src.substr(startpos, vertShader.length()) == vertShader) {
			type = GL_VERTEX_SHADER_ARB;
			startpos += vertShader.length();
			error = "compiling vertex";
		} else if(src.substr(startpos, fragmentShader.length()) == fragmentShader) {
			type = GL_FRAGMENT_SHADER_ARB;
			startpos += fragmentShader.length();
			error = "compiling fragment";
		} else {
			WARNING("loading shader \"%s\": unknown object type", file.string().c_str());
		}
		
		size_t pos = src.find(shaderSeperator, startpos);
		size_t nbytes;
		if(pos == string::npos) {
			nbytes = string::npos;
		} else {
			nbytes = pos - startpos;
			pos += shaderSeperator.length();
		}
		
		GLuint obj = glCreateShaderObjectARB(type);
		string objSrc = src.substr(startpos, nbytes);
		const char * data = objSrc.c_str();
		glShaderSourceARB(obj, 1, &data, NULL);
		glCompileShaderARB(obj);
		checkShader(obj, error, GL_OBJECT_COMPILE_STATUS_ARB, file);
		glAttachObjectARB(shader, obj);
		glDeleteObjectARB(obj);
		
		startpos = pos;
	}
	
	glLinkProgramARB(shader);
	checkShader(shader, "linking", GL_OBJECT_LINK_STATUS_ARB, file);
	
	string err = "creating shader (" + file.string() + ")";
	
	checkGl(err.c_str(), true);
	
	assert(shader);
	
	return shader;
}

bool Renderer::init(size_t width, size_t height) {
	
	GLenum error = glewInit();
	if(error != GLEW_OK) {
		ERROR("initializing glew: %d: %s", error, glewGetErrorString(error));
		return false;
	}
	
	_p->width = width;
	_p->height = height;
	
	glShadeModel(GL_SMOOTH);
	
	// Set up the rendering context, define display lists etc.:
	glClearColor(0.0,0.0,0.0,0.0);
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0);
	
	glEnable(GL_CULL_FACE);
	
	//glEnable(GL_BLEND);
	
	glEnable(GL_TEXTURE_2D);
	
	//glEnable(GL_NORMALIZE);
	
	//glEnable(GL_LINE_SMOOTH);
	//glEnable(GL_POLYGON_SMOOTH);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	//glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	
	if(!checkGl("initializing", true)) {
		return false;
	}
	
	
	if(!GLEW_ARB_shader_objects) {
		ERROR("missing required GL_ARB_shader_objects");
		return false;
	}
	if(!GLEW_ARB_fragment_program) {
		ERROR("missing required ARB_fragment_program");
		return false;
	}
	
	// Create the main rendering shader.
	_p->shader_render = loadShader("../render.glsl");
	
	
	if(!GLEW_EXT_framebuffer_object) {
		ERROR("missing required EXT_framebuffer_object");
		return false;
	}
	
	
	/* Perpare shadow map */
	
	glGenTextures(1, &_p->shadow_map);
	assert(_p->shadow_map);
	glBindTexture(GL_TEXTURE_2D, _p->shadow_map);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, SHADOWMAP_SIZE, SHADOWMAP_SIZE, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	if(!checkGl("creating shadowmap texture", true)) {
		return false;
	}
	
	// Create FBO and attach textures/buffers.
	glGenFramebuffersEXT(1, &_p->shadow_fbo);
	assert(_p->shadow_fbo);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _p->shadow_fbo);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, _p->shadow_map, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	if(!checkFBO("shadowmap")) {
		return false;
	}
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	
	if(!checkGl("creating shadowmap FBO", true)) {
		return false;
	}
	
	
	/* Pepare post processing texture */
	
	if(!_p->pps.empty()) {
		
		glGenTextures(_p->pps.size() > 1 ? 2 : 1, _p->pp_tex);
		assert(_p->pp_tex[0]);
		glBindTexture(GL_TEXTURE_2D, _p->pp_tex[0]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
		if(_p->pps.size() > 1) {
			glBindTexture(GL_TEXTURE_2D, _p->pp_tex[1]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
		
		glBindTexture(GL_TEXTURE_2D, 0);
		
		glGenRenderbuffersEXT(1, &_p->pp_rb);
		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, _p->pp_rb);
		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);
		
		if(!checkGl("creating pp textures", true)) {
			return false;
		}
		
		glGenFramebuffersEXT(1, &_p->pp_fbo);
		assert(_p->pp_fbo);
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _p->pp_fbo);
		_p->generatePPTextures();
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, _p->pp_tex[0], 0);
		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, _p->pp_rb);
		if(!checkFBO("postprocessing")) {
			return false;
		}
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
		
		if(!checkGl("creating pp FBO", true)) {
			return false;
		}
		
		// Create the postprocessing shaders.
		OUT("Using postprocessing shader%s", _p->pps.size() > 1 ? "s" : "");
		BOOST_FOREACH(const string & name, _p->pps) {
			string fn = "../postprocess_" + name + ".glsl";
			OUT(" - " OUT_COLORED(OUT_BLUE, "%s"), fn.c_str());
			_p->shader_pp.push_back(loadShader(fn.c_str()));
		}
		assert(_p->shader_pp.size() == _p->pps.size());
		
	}
	
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluPerspective(90.0, 1.0, 0.01, 1000.0);
	glGetFloatv(GL_PROJECTION_MATRIX, _p->lightProj);
	glPopMatrix();
	
	return true;
}


void Renderer::resized(size_t w, size_t h) {
	
	_p->width = w;
	_p->height = h;
	
	// setup viewport, projection etc.:
	glViewport(0, 0, (GLint)w, (GLint)h);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	float aspect = ((float)w) / h;
	
	gluPerspective(60.0, aspect, 0.01, 1000.0);
	
	_p->generatePPTextures();
	
}

void transformToGl(const transform & trans, GLfloat glmat[16]) {
	glmat[ 0] = trans.r.d[0][0];
	glmat[ 1] = trans.r.d[1][0];
	glmat[ 2] = trans.r.d[2][0];
	glmat[ 3] = 0.0f;
	glmat[ 4] = trans.r.d[0][1];
	glmat[ 5] = trans.r.d[1][1];
	glmat[ 6] = trans.r.d[2][1];
	glmat[ 7] = 0.0f;
	glmat[ 8] = trans.r.d[0][2];
	glmat[ 9] = trans.r.d[1][2];
	glmat[10] = trans.r.d[2][2];
	glmat[11] = 0.0f;
	glmat[12] = trans.t.x;
	glmat[13] = trans.t.y;
	glmat[14] = trans.t.z;
	glmat[15] = 1.0f;
}

void Renderer::renderShadowmap(const World & world) {
	
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _p->shadow_fbo);
	
	glUseProgram(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glCullFace(GL_FRONT);
	
	glViewport(0, 0, SHADOWMAP_SIZE, SHADOWMAP_SIZE);
	
	glClear(GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadMatrixf(_p->lightProj);
	
	glMatrixMode(GL_MODELVIEW);
	GLfloat lightView[16];
	transformToGl(world.getLightPosition(), lightView);
	glLoadMatrixf(lightView);
	
	vec lightpos = world.getLightPosition()(v_null());
	world.render(lightpos);
	
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	
	checkGl("rendering shadowmap");
	
}

void drawSphere();

void Renderer::renderScene(const transform & camera, const World & world) {
	
	if(_p->shader_pp.empty()) {
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	} else {
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _p->pp_fbo);
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, _p->pp_tex[0], 0);
	}
	
	// Moving from unit cube [-1,1] to [0,1]  
	const float bias[16] = {
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	};
	
	GLfloat glmat[16];
	
	glViewport(0, 0, _p->width, _p->height);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glCullFace(GL_BACK);
	
	glMatrixMode(GL_MODELVIEW);
	transformToGl(camera, glmat);
	glLoadMatrixf(glmat);
	
	// view -> model -> light -> projection -> texture matrix
	glMatrixMode(GL_TEXTURE);
	glActiveTexture(GL_TEXTURE0);
	glLoadMatrixf(bias); // projection -> texture coordinates
	glMultMatrixf(_p->lightProj); // light projection
	transformToGl(world.getLightPosition(), glmat);
	glMultMatrixf(glmat); // model space -> light space
	transform invCamera = -camera;
	transformToGl(invCamera, glmat);
	glMultMatrixf(glmat); // view space -> model space
	
	// Go back to normal matrix mode
	glMatrixMode(GL_MODELVIEW);
	
	// Light position in view space.
	vec lightPos = world.getLightPosition()[v_null()];
	vec viewLightPos = invCamera[lightPos];
	
	// Setup the shader.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _p->shadow_map);
	glUseProgram(_p->shader_render);
	GLuint map_l = glGetUniformLocationARB(_p->shader_render, "shadowmap");
	glUniform1iARB(map_l, 0);
	GLuint lightPos_l = glGetUniformLocationARB(_p->shader_render, "lightPos");
	glUniform3f(lightPos_l, viewLightPos.x, viewLightPos.y, viewLightPos.z);
	
	// Paint the scene.
	vec view_pos = invCamera(v_null());
	vec orig_view_pos = view_pos;/*
	vec view_dir = invCamera.r(v_z()).normal();
	float height = (world.heightAt(view_pos.x, view_pos.z) - view_pos.y);
	height = sqrt(height * height);
	
	cout << "---------------------" << endl << "view_dir: (" << view_dir.x << ", " << view_dir.y << ", " << view_dir.z << ')' << endl;
	
	float delta = 0.5f;
	for(size_t i = 0; i < 64; i++) {
		cout << i << ": (" << view_pos.x << ", " << view_pos.y << ", " << view_pos.z << ") score=" << height << " delta=" << delta << endl;
		vec new_pos = view_pos + (view_dir * delta);
		float new_height = (world.heightAt(new_pos.x, new_pos.z) - new_pos.y);
		new_height = sqrt(new_height * new_height);
		if(new_height < height) {
			view_pos = new_pos, height = new_height;
			delta *= 1.5f;
		} else {
			delta *= .75f;
			if(delta < 0.2f) {
				delta = -delta;
			}
		}
		
	}
	
	cout << "view_dir: (" << view_dir.x << ", " << view_dir.y << ", " << view_dir.z << ')' << endl << "---------------------" << endl ;
	
	glColor3f(0.f, 0.f, 0.f);
	glBegin(GL_LINES);
		glVertex3f(orig_view_pos.x, orig_view_pos.y, orig_view_pos.z);
		glVertex3f(view_pos.x, view_pos.y, view_pos.z);
	glEnd();
	glPushMatrix();
	glTranslatef(- view_pos.x, orig_view_pos.y, -view_pos.z);
	drawSphere();
	glPopMatrix();*/
	
	vec newpos(view_pos.x, orig_view_pos.y, view_pos.z);
	
	static vec nowpos = newpos;
	static size_t count = 0;
	if(count == 0) {
		count = 0;
		nowpos = newpos;
	} else {
		count++;
	}
	
	world.render(nowpos);
	
	// Visualize the light source.
	glColor3f(1, 1, 1);
	glTranslatef(lightPos.x, lightPos.y, lightPos.z);
	GLUquadric * quadr =  gluNewQuadric();
	gluSphere(quadr, 0.1, 20, 20);
	gluDeleteQuadric(quadr);
	
	glMatrixMode(GL_TEXTURE);
	glActiveTexture(GL_TEXTURE0);
	glLoadIdentity();
	
	checkGl("rendering scene");
	
}

void Renderer::_Private::renderPostprocessing(GLuint shader, float radius, float var, float time) {
	
	glViewport(0, 0, (GLint)width, (GLint)height);
	
	glClear(GL_COLOR_BUFFER_BIT);
	
	// Setup the shader.
	glUseProgram(shader);
	GLuint render_l = glGetUniformLocationARB(shader, "render");
	glUniform1iARB(render_l, 0);
	GLuint radius_l = glGetUniformLocationARB(shader, "radius");
	glUniform1fARB(radius_l, radius);
	GLuint var_l = glGetUniformLocationARB(shader, "var");
	glUniform1fARB(var_l, var);
	GLuint time_l = glGetUniformLocationARB(shader, "time");
	glUniform1fARB(time_l, (float)time/2);
	float aspect = ((float)width) / height;
	GLuint aspect_l = glGetUniformLocationARB(shader, "aspect");
	glUniform1fARB(aspect_l, aspect);
	
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, 1, 0, 1, -1, 1);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);
		
		glTexCoord2f(0, 0);
		glVertex3f(0, 0, 0);
		
		glTexCoord2f(1, 0);
		glVertex3f(1, 0, 0);
		
		glTexCoord2f(1, 1);
		glVertex3f(1, 1, 0);
		
		glTexCoord2f(0, 1);
		glVertex3f(0, 1, 0);
		
	glEnd();
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	
}

void Renderer::postprocess(float radius, float var, float time) {
	
	int s = _p->shader_pp.size();
	
	glActiveTexture(GL_TEXTURE0);
	for(int i = 0; i < s; i++) {
		glBindTexture(GL_TEXTURE_2D, _p->pp_tex[i % 2]);
		if(i + 1 == s) {
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
		} else {
			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, _p->pp_tex[(i+1) % 2], 0);
		}
		checkGl("setting up FBO");
		_p->renderPostprocessing(_p->shader_pp[i], radius, var, time);
		string err = "postprocessing (" + _p->pps[i] + ")";
		checkGl(err.c_str());
	}
	
}
