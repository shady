
#ifndef SHADY_TERRAIN_HPP
#define SHADY_TERRAIN_HPP

#include "transform.hpp"

#include <vector>

class terrain{
	
public:
	
	terrain(int res, float size, float height);
	~terrain();
	
	void generate(int randres, float divisor, float power, int seed);
	
	void drawGl() const;
	
	void generateVertices();
	
	void drawVertices(vec pos) const;
	
	float heightAt(float x, float y) const;
	
private:
	
	struct Vertex {
		
		vec pos;
		
		vec normal;
		
	};
	
	int res;
	float size;
	float height;
	
	float ** data;
	
	float xVal(int x, int y) const;
	float yVal(int x, int y) const;
	float zVal(int x, int y) const;
	
	void drawGlVertex(int x, int y) const;
	
	static void setVertexArray(Vertex * vert);
	
	enum ToCenter {
		X0 = 1,
		X1 = 2,
		Y0 = 4,
		Y1 = 8,
		None
	};
	
	void addRect(std::vector<unsigned short> & d, int x0, int x1, int y0, int y1, vec pos, ToCenter tc) const;
	
	bool isSubdivided(int x0, int x1, int y0, int y1, vec pos) const;
	
	Vertex * vertices;
	
};

#endif // SHADY_TERRAIN_HPP
