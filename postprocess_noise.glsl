
@@@ FRAGMENT SHADER

uniform sampler2D render;


uniform float radius;
uniform float var;
uniform float time;

void main() {
	vec2 p = gl_TexCoord[0].st;
	vec4 color = texture2D(render, p);

	//generate noise
	int a = 12547831;
	a += int(1254.2654*time);
	a -= a<<4;
	a ^= a>>3;
	a ^= a<<9;
	a += a>>3;
	a -= int(1254.2654*p.x);
	a ^= a<<7;
	a ^= a>>5;
	a += a<<3;
	a -= a>>6;
	a ^= int(1254.2654*p.y);
	a ^= a<<5;
	a += a>>3;
	a -= a<<2;
	a ^= a>>9;
	float noise = float((a%1483))/1484;
	float avnoise = 2*noise-1;

	float softnoise = 0.5+0.5*noise;

	gl_FragColor = color + 0.5*var*noise*color + 0.1*radius*avnoise*vec4(1,1,1,0);
}

