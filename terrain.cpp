
#include "terrain.hpp"
#include "transform.hpp"
#include "renderer.hpp"
extern "C" {
#include <GL/glew.h>
#include <GL/gl.h>
}

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <boost-1_46/boost/config/no_tr1/complex.hpp>
using std::cout;
using std::endl;

float randdiff(float scale,float power) {
	float tmp = ((float)rand())/RAND_MAX; //value between 0 and 1
	//cerr << "in: " << tmp <<"\n";
	tmp = (tmp*2-1); // between -1 and 1
	bool neg = false;
	if(tmp < 0) {
		neg = true;
		tmp *= -1;
	}
	tmp = pow(tmp, power);
	if(neg) {
		tmp *=-1;
	}
	//cerr << "out: " << tmp <<"\n";
	return tmp*scale;
}

int mod(int a, int b) {
	int v = a;
	if(v < 0) {
		v = b - a;
	}
	return v % b;
}

terrain::terrain(int res, float size, float height) : vertices(NULL) {
	this->res = res;
	this->height = height;
	this->size = size;
	
	// cout << "allocating trrn ram  size:" << res*res*sizeof(float) << endl;
	// allocate RAM
	data = new float*[res];
	for(int x = 0; x < res; x++) {
		data[x] = new float[res];
		for(int y = 0; y < res; y++) {
			data[x][y] = 0;
		}
	}
	// cout << "done allocating" << endl;
}

terrain::~terrain() {
	//free RAM
	for(int i = 0; i < res; i++) {
		delete data[i];
	}
	delete data;
}

void terrain::generate(int randres, float divisor, float power, int seed){
	srand(seed);
	cout << "generating trrn map" << endl;
	//clear map
	for(int x = 0; x < res; x++) for(int y = 0; y < res; y++){
		data[x][y] = 0;
	}
	
	//initialize randres with random values
	for(int x = 0; x < res; x += res/randres) for(int y = 0; y < res; y += res/randres) {
		data[x][y] = 0.5 + randdiff(0.3, power);
	}
	
	float coef = .3;
	int s;
	for(int level = randres; level < res; level *= 2) {
		s = res/level/2;
		coef /= divisor;
		//diamond
		for(int x = res/(2*level); x < res; x += res/level) for(int y = res/(2*level); y < res; y += res/level) {
			data[x][y] = 
			  (data[mod(x+s, res)][mod(y+s, res)]
			 + data[mod(x+s, res)][mod(y-s, res)]
			 + data[mod(x-s, res)][mod(y+s, res)]
			 + data[mod(x-s, res)][mod(y-s, res)]) / 4
			 + randdiff(coef,power);
		}
		coef /= divisor;
		//square
		for(int x = 0; x < res; x += res/(2*level)) for(int y = 0; y < res; y += res/(2*level))
		if((x % (res/level)) ^ (y % (res/level))){
			//cerr << "adding " << x << "x" << y << "\n";
			data[x][y] = 
			  (data[mod(x+s, res)][y]
			 + data[mod(x-s, res)][y]
			 + data[x][mod(y+s, res)]
			 + data[x][mod(y-s, res)]) / 4
			 + randdiff(coef, power);
		}
		
	}
	cout << "done generating" << endl;
}

float terrain::xVal(int x, int y) const {
	return (float)x * size / (float)res - size/2;
}

float terrain::yVal(int x, int y) const {
	return height * data[mod(x, res)][mod(y, res)];
}

float terrain::zVal(int x, int y) const {
	return (float)y * size / (float)res - size/2;
}

float interpolWeight(float x, float y) {
	return std::sqrt((x * x) + (y * y)) + 0.00001;
}

float terrain::heightAt(float x, float y) const {
	float a = (x + size/2) * (float)res / size;
	float b = (y + size/2) * (float)res / size;
	
	//rounded position
	int ra = (int)a;
	int rb = (int)b;
	
	//differences
	// TODO interpolate
	float tmp;
	tmp =  yVal(ra, rb)*interpolWeight(ra-a,rb-b)
	      +yVal(ra+1, rb)*interpolWeight(ra+1-a,rb-b)
	      +yVal(ra, rb+1)*interpolWeight(ra-a,rb+1-b)
	      +yVal(ra+1, rb+1)*interpolWeight(ra-a,rb+1-b);
	tmp /= interpolWeight(ra-a,rb-b)
	      +interpolWeight(ra+1-a,rb-b)
	      +interpolWeight(ra-a,rb+1-b)
	      +interpolWeight(ra-a,rb+1-b);
	return tmp;
}

#define terrainindex(x, y) ((res * mod((y), res)) + mod((x), res))

void terrain::generateVertices() {
	
	if(vertices) {
		delete[] vertices;
	}
	vertices = new Vertex[res * res];
	
	glBufferData(GL_ARRAY_BUFFER, res * res * sizeof(Vertex), NULL, GL_STATIC_DRAW);
	
	Vertex * vert = reinterpret_cast<Vertex *>(glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
	
	for(int y = 0; y < res; y++) for(int x = 0; x < res; x++) {
		
		vec rect[4] = {
			vec(xVal(x+1, y-1), yVal(x+1, y-1), zVal(x+1, y-1)),
			vec(xVal(x-1, y-1), yVal(x-1, y-1), zVal(x-1, y-1)),
			vec(xVal(x-1, y+1), yVal(x-1, y+1), zVal(x-1, y+1)),
			vec(xVal(x+1, y+1), yVal(x+1, y+1), zVal(x+1, y+1))
		};
		
		Vertex & v = vertices[terrainindex(x, y)];
		
		v.pos = vec(xVal(x, y), yVal(x, y), zVal(x, y));
		v.normal = calculateNormal(rect, 4);
		
		vert[terrainindex(x, y)] = v;
	}
	
	glUnmapBuffer(GL_ARRAY_BUFFER);
	
	checkGl("adding terrain vertices");
}

void terrain::setVertexArray(Vertex * vert) {
	
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	
	glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &vert->pos);
	glNormalPointer(GL_FLOAT, sizeof(Vertex), &vert->normal);
	
}

bool terrain::isSubdivided(int x0, int x1, int y0, int y1, vec pos) const {
	
	if(x1 - x0 <= 1 || y1 - y0 <= 1) {
		return false;
	}
	
	int w = (x1 - x0), h = (y1 - y0);
	int mx = ((w / 2) + x0),  my = ((h / 2) + y0);
	vec center(xVal(mx, my), yVal(mx, my), zVal(mx, my));
	
	float sfacor = res / float(sqrt(w * h));
	
	vec diff = (pos - center);
	diff.y /= 2;
	float d = diff.det();
	if(d > 7.f) {
		d -= 7.f;
	} else {
		d = 0.f;
	}
	
	return (d * sfacor < 300.f);
}

void terrain::addRect(std::vector<unsigned short> & ind, int x0, int x1, int y0, int y1, vec pos, ToCenter tc) const {
	
	int w = (x1 - x0), h = (y1 - y0);
	int mx = (x0 + (w/2)),  my = (y0 + (h/2));
	
	if(isSubdivided(x0, x1, y0, y1, pos)) {
		
		// subdivide
		
		addRect(ind, x0, mx, y0, my, pos, None);
		addRect(ind, x0, mx, my, y1, pos, None);
		addRect(ind, mx, x1, my, y1, pos, None);
		addRect(ind, mx, x1, y0, my, pos, None);
		
	} else {
		
		GLbyte * c = reinterpret_cast<GLbyte *>(&data[mod(x0, res)][mod(y0, res)]);
		glColor3f(float(c[0])/256 + .5f, float(c[1])/256 + .5f, float(c[2])/256 + .5f);
		
		int tvert = 0;
		tvert |= ((tc == X0) || isSubdivided(x0 - w, x0, y0, y1, pos)) ? X0 : 0;
		tvert |= ((tc == X1) || isSubdivided(x1, x1 + w, y0, y1, pos)) ? X1 : 0;
		tvert |= ((tc == Y0) || isSubdivided(x0, x1, y0 - h, y0, pos)) ? Y0 : 0;
		tvert |= ((tc == Y1) || isSubdivided(x0, x1, y1, y1 + h, pos)) ? Y1 : 0;
		
		switch(tvert) {
			
			case 0: {
				
				glBegin(GL_QUADS);
					
					drawGlVertex(x0, y0);
					drawGlVertex(x0, y1);
					drawGlVertex(x1, y1);
					drawGlVertex(x1, y0);
					
				glEnd();
				
				break;
			}
			
			case X0: {
				
				glBegin(GL_TRIANGLES);
					
					drawGlVertex(x1, y0);
					drawGlVertex(x0, y0);
					drawGlVertex(x0, my);
					
					drawGlVertex(x0, my);
					drawGlVertex(x1, y1);
					drawGlVertex(x1, y0);
					
					drawGlVertex(x0, my);
					drawGlVertex(x0, y1);
					drawGlVertex(x1, y1);
					
				glEnd();
				
				break;
			}
			
			case X0 | Y1: {
				
				glColor3f(1.f, 1.f, 1.f);
				
				glBegin(GL_TRIANGLES);
					
					drawGlVertex(x0, my);
					drawGlVertex(mx, y1);
					drawGlVertex(x1, y0);
					
					drawGlVertex(x0, my);
					drawGlVertex(x0, y1);
					drawGlVertex(mx, y1);
					
					drawGlVertex(x1, y0);
					drawGlVertex(mx, y1);
					drawGlVertex(x1, y1);
					
					drawGlVertex(x0, my);
					drawGlVertex(x1, y0);
					drawGlVertex(x0, y0);
					
				glEnd();
				
				break;
			}
			
			case X0 | Y0: {
				
				glColor3f(1.f, 1.f, 1.f);
				
				glBegin(GL_TRIANGLES);
					
					drawGlVertex(x0, my);
					drawGlVertex(x1, y1);
					drawGlVertex(mx, y0);
					
					drawGlVertex(x0, my);
					drawGlVertex(mx, y0);
					drawGlVertex(x0, y0);
					
					drawGlVertex(x1, y1);
					drawGlVertex(x1, y0);
					drawGlVertex(mx, y0);
					
					drawGlVertex(x0, my);
					drawGlVertex(x0, y1);
					drawGlVertex(x1, y1);
					
				glEnd();
				
				break;
			}
			
			case X0 | Y0 | Y1: {
				
				
				break;
			}
			
			case X0 | X1: {
				
				
				break;
			}
			
			
			case X0 | X1 | Y1: {
				
				
				break;
			}
			
			case X0 | X1 | Y0: {
				
				
				break;
			}
			
			case X0 | X1 | Y0 | Y1: {
				
				
				break;
			}
			
			
			case X1: {
				
				glBegin(GL_TRIANGLES);
					
					drawGlVertex(x1, y0);
					drawGlVertex(x0, y0);
					drawGlVertex(x1, my);
					
					drawGlVertex(x1, my);
					drawGlVertex(x0, y0);
					drawGlVertex(x0, y1);
					
					drawGlVertex(x1, my);
					drawGlVertex(x0, y1);
					drawGlVertex(x1, y1);
					
				glEnd();
				
				
				break;
			}
			
			case X1 | Y1: {
				
				glColor3f(1.f, 1.f, 1.f);
				
				glBegin(GL_TRIANGLES);
					
					drawGlVertex(x1, my);
					drawGlVertex(x0, y0);
					drawGlVertex(mx, y1);
					
					drawGlVertex(x1, my);
					drawGlVertex(mx, y1);
					drawGlVertex(x1, y1);
					
					drawGlVertex(x0, y0);
					drawGlVertex(x0, y1);
					drawGlVertex(mx, y1);
					
					drawGlVertex(x1, my);
					drawGlVertex(x1, y0);
					drawGlVertex(x0, y0);
					
				glEnd();
				
				
				break;
			}
			
			case X1 | Y0: {
				
				glColor3f(1.f, 1.f, 1.f);
				
				glBegin(GL_TRIANGLES);
					
					drawGlVertex(x1, my);
					drawGlVertex(mx, y0);
					drawGlVertex(x0, y1);
					
					drawGlVertex(x1, my);
					drawGlVertex(x1, y0);
					drawGlVertex(mx, y0);
					
					drawGlVertex(x0, y1);
					drawGlVertex(mx, y0);
					drawGlVertex(x0, y0);
					
					drawGlVertex(x1, my);
					drawGlVertex(x0, y1);
					drawGlVertex(x1, y1);
					
				glEnd();
				
				
				break;
			}
			
			case X1 | Y0 | Y1: {
				
				
				break;
			}
			
			case Y0: {
				
				glBegin(GL_TRIANGLES);
					
					drawGlVertex(x0, y0);
					drawGlVertex(x0, y1);
					drawGlVertex(mx, y0);
					
					drawGlVertex(mx, y0);
					drawGlVertex(x0, y1);
					drawGlVertex(x1, y1);
					
					drawGlVertex(mx, y0);
					drawGlVertex(x1, y1);
					drawGlVertex(x1, y0);
					
				glEnd();
				
				
				break;
			}
			
			case Y0 | Y1: {
				
				
				break;
			}
			
			case Y1: {
				
				glBegin(GL_TRIANGLES);
					
					drawGlVertex(x0, y0);
					drawGlVertex(x0, y1);
					drawGlVertex(mx, y1);
					
					drawGlVertex(mx, y1);
					drawGlVertex(x1, y0);
					drawGlVertex(x0, y0);
					
					drawGlVertex(mx, y1);
					drawGlVertex(x1, y1);
					drawGlVertex(x1, y0);
					
				glEnd();
				
				
				break;
			}
			
		}
		
		
		/*
		ind.push_back(terrainindex(x0, y0));
		ind.push_back(terrainindex(x0, y1));
		ind.push_back(terrainindex(x1, y1));
		ind.push_back(terrainindex(x1, y0));*/
		
	}
	
	
}

void terrain::drawVertices(vec pos) const {
	
	glDisable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	glLineWidth(.5f);
	
	std::vector<unsigned short> indices;
	
	indices.reserve(1000);
	
	// rounded position
	float a = (pos.x + size/2) * (float)res / size;
	float b = (pos.z + size/2) * (float)res / size;
	int ix = int(a + .5f), iy = int(b + .5f);
	
	int depth = abs(heightAt(pos.x, pos.z) - pos.y);
	if(depth < 1) {
		depth = 1;
	}
	size_t r = 2 << (size_t)log(depth * 2);
	
	addRect(indices, ix - r, ix,     iy - r, iy    , pos, None);
	addRect(indices, ix,     ix + r, iy - r, iy    , pos, None);
	addRect(indices, ix,     ix + r, iy,     iy + r, pos, None);
	addRect(indices, ix - r, ix,     iy,     iy + r, pos, None);
	
	for(int i = 0; indices.size() < 750 && i < 20; r *= 2, i++) {
		
		size_t r2 = r * 2;
		
		addRect(indices, ix - r2, ix - r,  iy - r2, iy - r,  pos, None);
		addRect(indices, ix - r,  ix,      iy - r2, iy - r,  pos, i == 0 ? None : Y1);
		addRect(indices, ix,      ix + r,  iy - r2, iy - r,  pos, i == 0 ? None : Y1);
		addRect(indices, ix + r,  ix + r2, iy - r2, iy - r,  pos, None);
		addRect(indices, ix + r,  ix + r2, iy - r,  iy    ,  pos, i == 0 ? None : X0);
		addRect(indices, ix + r,  ix + r2, iy,      iy + r,  pos, i == 0 ? None : X0);
		addRect(indices, ix + r,  ix + r2, iy + r,  iy + r2, pos, None);
		addRect(indices, ix,      ix + r,  iy + r,  iy + r2, pos, i == 0 ? None : Y0);
		addRect(indices, ix - r,  ix,      iy + r,  iy + r2, pos, i == 0 ? None : Y0);
		addRect(indices, ix - r2, ix - r,  iy + r,  iy + r2, pos, None);
		addRect(indices, ix - r2, ix - r,  iy,      iy + r,  pos, i == 0 ? None : X1);
		addRect(indices, ix - r2, ix - r,  iy - r,  iy    ,  pos, i == 0 ? None : X1);
		
	}
	
	// setVertexArray(NULL);
	
	glColor3f(0.5, 0.4, 0.2);
	
	//glDrawElements(GL_QUADS, indices.size(), GL_UNSIGNED_SHORT, &indices.front());
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	checkGl("rendering terrain vertices");
}

void terrain::drawGlVertex(int x, int y) const {
	
	/*float nx = yVal(x-1,y) - yVal(x+1,y); //Height[x-1][y] - Height[x+1][y];
	float nz = 4 / (size/res);
	float ny = yVal(x,y-1) - yVal(x,y+1); //Height[x][y-1] - Height[x][y+1];*/
	/*vec rect[4] = {
		vec(xVal(x+1, y-1), yVal(x+1, y-1), zVal(x+1, y-1)),
		vec(xVal(x-1, y-1), yVal(x-1, y-1), zVal(x-1, y-1)),
		vec(xVal(x-1, y+1), yVal(x-1, y+1), zVal(x-1, y+1)),
		vec(xVal(x+1, y+1), yVal(x+1, y+1), zVal(x+1, y+1))
	};*/
	
	
	//glColor3f(0.5, 0.4, 0.2);
		//glColor3f(x / res + .1f, y / res + .1f, randVal() + .1f);
	//vec normal = calculateNormal(rect,4);
	const vec & normal = vertices[terrainindex(x, y)].normal;
	glNormal3f(normal.x, normal.y, normal.z);
	
	
	vec pos(xVal(x, y), yVal(x, y), zVal(x, y));
	//const vec & pos = vertices[terrainindex(x, y)].pos;
	glVertex3f(pos.x, pos.y, pos.z);
}

void terrain::drawGl() const {
	glBegin(GL_QUADS);
	for(int y = 0; y < res; y++) for(int x = 0; x < res; x++) {
		drawGlVertex(x + 0, y + 0);
		drawGlVertex(x + 0, y + 1);
		drawGlVertex(x + 1, y + 1);
		drawGlVertex(x + 1, y + 0);
	}
	glEnd();
}
