
#ifndef SHADY_RENDERER_HPP
#define SHADY_RENDERER_HPP

#include <vector>
#include <string>

class transform;
class World;


class Renderer {
	
public:
	
	Renderer(const std::vector<std::string> & pps);
	
	~Renderer();
	
	/**
	 * Initialize the rendering system.
	 */
	bool init(size_t width, size_t height);
	
	void renderShadowmap(const World & world);
	
	/**
	 * Render one frame.
	 * @param camera The transformation from model space to view space.
	 **/
	void renderScene(const transform & camera, const World & world);
	
	void postprocess(float radius, float var, float time);
	
	/**
	 * Set a new render size.
	 */
	void resized(size_t width, size_t height);
	
private:
	
	class _Private;
	_Private * const _p;
	
};

bool checkGl(const char * action, bool fatal = false);

#endif // SHADY_RENDERER_HPP
