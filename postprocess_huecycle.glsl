
@@@ FRAGMENT SHADER

uniform sampler2D render;


uniform float radius;
uniform float var;
uniform float time;

void main() {
	float c = pow(cos(time),2);
	float s = pow(sin(time),2);
	vec2 p = gl_TexCoord[0].st;
	vec4 src = texture2D(render, p);
	gl_FragColor =vec4(s*src.y+c*src.z, s*src.z+c*src.x, s*src.x+c*src.y, 0);
}

