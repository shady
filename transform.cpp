
#include "transform.hpp"

#include <cmath>
#include <cstdlib>

using std::sin;
using std::cos;
using std::rand;

float randVal() {
	return ((float)std::rand()) / RAND_MAX;
}

float randVar(float val, float var) {
	
	if(var == 0.0) {
		return val;
	}
	
	return val + (((randVal() * 2) - 1) * var);
}

int randNextInt(float val) {
	
	float proportion = val - floor(val);
	
	if(randVal() < proportion) {
		return floor(val);
	} else {
		return ceil(val);
	}
	
}

float randVal(float min, float max) {
	return (randVal() * (max - min)) + min;
}

/** Matrix */

#define T(r,t,i) ((r[i][0] * t.x) + (r[i][1] * t.y) + (r[i][2] * t.z))

#define TI(r,t,i) ((r[0][i] * t.x) + (r[1][i] * t.y) + (r[2][i] * t.z))


#define S(i,j,k) (d[i][k] * b.d[k][j])
#define M(i,j)   (S(i,j,0) + S(i,j,1) + S(i,j,2))

mat::mat(const mat & o) {
	d[0][0] = o.d[0][0]; d[0][1] = o.d[0][1]; d[0][2] = o.d[0][2];
	d[1][0] = o.d[1][0]; d[1][1] = o.d[1][1]; d[1][2] = o.d[1][2];
	d[2][0] = o.d[2][0]; d[2][1] = o.d[2][1]; d[2][2] = o.d[2][2];
}

vec mat::operator()(const vec & v) const {
	return vec(T(d,v,0), T(d,v,1), T(d,v,2));
}

vec mat::operator[](const vec & v) const {
	return vec(TI(d,v,0), TI(d,v,1), TI(d,v,2));
}

mat mat::operator*(const mat & b) const {
	return mat(M(0,0), M(0,1), M(0,2),
	           M(1,0), M(1,1), M(1,2),
	           M(2,0), M(2,1), M(2,2));
}

mat mat::operator-() const {
	return mat(d[0][0], d[1][0], d[2][0],
	           d[0][1], d[1][1], d[2][1],
	           d[0][2], d[1][2], d[2][2]);
}

mat m_null() { return mat(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0); }
mat m_id() { return mat(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0); }


/** Affine Transformation */

transform::transform(const mat & rot, const vec & trans) : r(rot), t(trans) { }

transform::transform(const transform & other) : r(other.r), t(other.t) { }

transform::transform() : r(m_id()), t(v_null()) { }

transform::transform(const mat & rot) : r(rot), t(v_null()) { }

transform::transform(const vec & trans): r(m_id()), t(trans) { }


inline void swap(float d[3][3], int i1, int j1, int i2, int j2) {
	register float t = d[i1][j1];
	d[i1][j1] = d[i2][j2];
	d[i2][j2] = t;
	// TODO use std::swap
}

transform & transform::invert() {
	swap(r.d, 0, 1, 1, 0);
	swap(r.d, 0, 2, 2, 0);
	swap(r.d, 1, 2, 2, 1);
	t = -r(t);
	return *this;
}

transform t_rotateX(float theta) {
	float s = sin(theta);
	float c = cos(theta);
	return transform(mat(1.0, 0.0, 0.0,
	                     0.0,   c,  -s,
	                     0.0,   s,   c));
}

transform t_rotateY(float theta) {
	float s = sin(theta);
	float c = cos(theta);
	return transform(mat(  c, 0.0,   s,
	                     0.0, 1.0, 0.0,
	                      -s, 0.0,   c));
}

transform t_rotateZ(float theta) {
	float s = sin(theta);
	float c = cos(theta);
	return transform(mat(  c,  -s, 0.0,
	                       s,   c, 0.0,
	                     0.0, 0.0, 1.0));
}

transform t_rotate(const vec & axis, float theta) {
	float s = sin(theta);
	float c = cos(theta);
	float x = axis.x;
	float y = axis.y;
	float z = axis.z;
	return transform(mat((x*x) + ((1 - (x*x))*c), (x*y*(1 - c)) - (z*s)  , (x*z*(1 - c)) + (y*s),
	                     (x*y*(1 - c)) + (z*s)  , (y*y) + ((1 - (y*y))*c), (y*z*(1 - c)) - (x*s),
	                     (x*z*(1 - c)) + (y*s)  , (y*z*(1 - c)) + (x*s)  , (z*z) + ((1 - (z*z))*c)));
}

vec calculateTriangleNormal(const vec p[3]) {
	
	vec u = p[1] - p[0];
	vec v = p[2] - p[0];
	
	vec normal;
	
	normal.x = (u.y * v.z) - (u.z * v.y);
	normal.y = (u.z * v.x) - (u.x * v.z);
	normal.z = (u.x * v.y) - (u.y * v.x);
	
	normal /= normal.det();
	
	return normal;
}

vec calculateNormal(const vec * p, int n) {
	
	vec normal(0.0, 0.0, 0.0);
	
	for(int i = 0; i < n; i++) {
		
		vec current = p[i];
		vec next = p[(i + 1) % n];
		
		normal.x += (current.y - next.y) * (current.z + next.z);
		normal.y += (current.z - next.z) * (current.x + next.x);
		normal.z += (current.x - next.x) * (current.y + next.y);
		
	}
	
	// normalize
	normal /= normal.det();
	
	return normal;
}