
#ifndef SHADY_SHADY_HPP
#define SHADY_SHADY_HPP

#include <vector>
#include <string>

class Shady {
	
public:
	
	Shady(const std::vector<std::string> & pps);
	
	~Shady();
	
	/**
	 * Initialize shady.
	 */
	bool init();
	
	/**
	 * Render one frame.
	 */
	void render();
	
	/**
	 * Set a new render size.
	 */
	void resized(int width, int height);
	
	/**
	 * A normal (character, number, ...) key has been pressed.
	 */
	void normalKeyPressed(unsigned char key, int x, int y);
	
	/**
	 * A special key has been pressed.
	 * 
	 * @param key They key that was pressed:
	 * GLUT_KEY_F1         F1 function key
	 * GLUT_KEY_F2         F2 function key
	 * GLUT_KEY_F3         F3 function key
	 * GLUT_KEY_F4         F4 function key
	 * GLUT_KEY_F5         F5 function key
	 * GLUT_KEY_F6         F6 function key
	 * GLUT_KEY_F7         F7 function key
	 * GLUT_KEY_F8         F8 function key
	 * GLUT_KEY_F9         F9 function key
	 * GLUT_KEY_F10        F10 function key
	 * GLUT_KEY_F11        F11 function key
	 * GLUT_KEY_F12        F12 function key
	 * GLUT_KEY_LEFT       Left function key
	 * GLUT_KEY_RIGHT      Right function key
	 * GLUT_KEY_UP         Up function key
	 * GLUT_KEY_DOWN       Down function key
	 * GLUT_KEY_PAGE_UP    Page Up function key
	 * GLUT_KEY_PAGE_DOWN  Page Down function key
	 * GLUT_KEY_HOME       Home function key
	 * GLUT_KEY_END        End function key
	 * GLUT_KEY_INSERT     Insert function key
	*/
	void specialKeyPressed(int key, int x, int y);
	
	/**
	 * The mouse has been moved while a key is pressed.
	 */
	void mouseDragged(int x, int y);
	
	/**
	 * A mouse button has been pressed or released.
	 * @param button The button that was pressed/released:
	 * GLUT_LEFT_BUTTON
	 * GLUT_MIDDLE_BUTTON
	 * GLUT_RIGHT_BUTTON
	 * @param state If the button was pressed or released: GLUT_DOWN or GLUT_UP
	 */
	void mouseClicked(int button, int state, int x, int y);
	
private:
	
	class _Private;
	_Private * const _p;
	
};

#endif // SHADY_SHADY_HPP
