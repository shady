
@@@ FRAGMENT SHADER

uniform sampler2D render;

uniform float radius;
uniform float var;
uniform float aspect;

void main() {
	
	vec2 p = gl_TexCoord[0].st;
	
	float r = radius * 0.005;
	
	vec4 s0 = texture2D(render, p);
	vec4 s1 = texture2D(render, p + vec2(0, r * aspect));
	vec4 s2 = texture2D(render, p + vec2(0, -r * aspect));
	vec4 s3 = texture2D(render, p + vec2(r, 0));
	vec4 s4 = texture2D(render, p + vec2(-r, 0));
	
	float v = - var * 10;
	gl_FragColor = (((2 - v) * s0) + (v * ((s1 + s2 + s3 + s4) / 4))) / 2;
	
}

