
@@@ FRAGMENT SHADER

uniform sampler2D render;


uniform float radius;
uniform float var;
uniform float time;

void main() {
	vec2 p = gl_TexCoord[0].st;
	

	//generate noise
	int a = 12547831;
	a += int(1254.2654*time);
	a -= a<<4;
	a ^= a>>3;
	a ^= a<<9;
	a += a>>3;
	a -= int(1254.2654*p.x);
	a ^= a<<7;
	a ^= a>>5;
	a += a<<3;
	a -= a>>6;
	a ^= int(1254.2654*p.y);
	a ^= a<<5;
	a += a>>3;
	a -= a<<2;
	a ^= a>>9;
	float hurlx = (2*float((a%1483))/1484)-1;
	a -= a<<3;
	a ^= a>>7;
	float hurly = (2*float((a%3591))/3590)-1;

	gl_FragColor = texture2D(render, p+0.01*var*vec2(hurlx,hurly));
}

